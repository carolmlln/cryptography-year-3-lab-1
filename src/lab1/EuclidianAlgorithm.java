package lab1;
/**
 * @author Carol Mullen D00196117
 */
import java.math.*;
import java.util.Arrays;
public class EuclidianAlgorithm 
{
    public static void main(String[] args)
    {
        //Declare variables for a, b, r -> the remainder and p -> atemp variable
        BigInteger a, b, r, p;
        //assign values to a & b
        a =  BigInteger.valueOf(4864);
        b =  BigInteger.valueOf(3458);
        //create a bigInteger array to hold the values calculated in the extended algorithm
        BigInteger[] vals = gcd(a, b);
        
        System.out.println("Euclidian Algorithm");
        System.out.println("===================================");
        while(b != BigInteger.valueOf(0))
        {
            p = a.divide(b);
            r = a.mod(b);
            //Show the calculations to get the gcd
            System.out.println(a + " = " + p + " * " + b + " + " +r);
            a = b;
            b = r;
        }
        //For the worked example above, this would return a = 38.
        System.out.println("\na = " + a);
        
        
        System.out.println("\n\nExtended Euclidian Algorithm");
        System.out.println("===================================");
        
        System.out.println("{ d, x, y } = " + Arrays.toString(vals));
        
        
    }
    
     //  return array [d, x, y] such that d = gcd(a, b), ax + by = d
    static BigInteger[] gcd(BigInteger a, BigInteger b)
    {
        if(b == BigInteger.valueOf(0))
            return new BigInteger[] {a, BigInteger.valueOf(1), BigInteger.valueOf(0)};
    
        BigInteger[] vals = gcd(b, a.mod(b));
        BigInteger d = vals[0];
        BigInteger x = vals[2];
        BigInteger y = vals[1].subtract((a.divide(b)).multiply(vals[2]));
        //System.out.println(d + ", " + x + ", " + y); //test to see calculations
        //For the worked example above, this would return x = 32, y = −45 and d = 38.
        return new BigInteger[] { d, x, y };
      
    }
    
}
