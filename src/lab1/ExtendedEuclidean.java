package lab1;

/**
 *
 * @author Carol Mullen D00196117
 */
import java.math.*;
import java.util.Arrays;

public class ExtendedEuclidean 
{
    public static void main(String[] args) 
    {
        BigInteger a, b;
        
        a =  BigInteger.valueOf(4864);
        b =  BigInteger.valueOf(3458);
        BigInteger[] vals = gcd(a, b);
        
        System.out.println("{ d, x, y } = " + Arrays.toString(vals));
    }
    
    //  return array [d, x, y] such that d = gcd(a, b), ax + by = d
    static BigInteger[] gcd(BigInteger a, BigInteger b)
    {
        if(b == BigInteger.valueOf(0))
            return new BigInteger[] {a, BigInteger.valueOf(1), BigInteger.valueOf(0)};
    
        BigInteger[] vals = gcd(b, a.mod(b));
        BigInteger d = vals[0];
        BigInteger x = vals[2];
        BigInteger y = vals[1].subtract((a.divide(b)).multiply(vals[2]));
        System.out.println(d + ", " + x + ", " + y);
        return new BigInteger[] { d, x, y };
      
    }


}
//For the worked example above, this would return x = 32, y = −45 and d = 38.

//INPUT: two non-negative integers a and b with a ≥ b
//OUTPUT: d = gcd(a, b) and integers x, y satisfying ax + by = d.
//1. if b = 0 then set d ← a, x ← 1, y ← 0, and return (d, x, y)
//2. Set x2 ← 1, x1 ← 0, y2 ← 0, y1 ← 1.
//3. While b > 0 do the following:
//• q ← ba/bc, r ← a − qb, x ← x2 − qx1, y ← y2 − qy1
//• a, b ← r, x2 ← x1, x1 ← x, y2 ← y1 and y1 ← y.
//4. Set d ← a, x ← x2, y ← y2, and return (d, x, y).