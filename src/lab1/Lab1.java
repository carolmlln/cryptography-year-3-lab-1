package lab1;

/**
 *
 * @author D00196117 Carol Mullen
 */
public class Lab1 
{
    public static void main(String[] args) 
    {
       int a = 4864, b = 3458, r, p;
       
       while(b != 0)
       {
           p = a / b;
           r = a % b;
           System.out.println(a + " = " + p + " * " + b + " + " +r);
           a = b;
           b = r;
           
       }
       System.out.println(a);
    }
    
}
/**
 * Example (Euclidean algorithm)
The following are the division steps of Algorithm 1.1 for computing gcd(4864, 3458)
        4864 = 1 · 3458 + 1406
        3458 = 2 · 1406 + 646
        646 = 5 · 114 + 76
        114 = 1 · 76 + 38
        76 = 2 · 38 + 0

 */